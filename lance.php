<?php
/*******************************************************************************************************************************************/
/*
/*              PROGRAMME D'EXPORT D'UNE LISTE DE PUBLICATIONS A PARTIR D'UN idHAL POUR INRAE
/*
/*   Résultat : production d'un fichier excel dans le répertoire ./xlsx/ comportant les notices résultantes de l'interrogation de l'API HAL
/*              avec la requête suivante : https://api.archives-ouvertes.fr/search/inrae/?q=abstract_t:((scales%20OR%20echelles%20OR%20%22organisation%20levels%22%20OR%20%22organization%20levels%22%20OR%20%22organisational%20levels%22%20OR%20%22organizational%20levels%22%20OR%20%22niveaux%20d%27organisation%22)%20AND%20ecolog*)%20OR%20title_t:((scales%20OR%20echelles%20OR%20%22organisation%20levels%22%20OR%20%22organization%20levels%22%20OR%20%22organisational%20levels%22%20OR%20%22organizational%20levels%22%20OR%20%22niveaux%20d%27organisation%22)%20AND%20ecolog*)%20OR%20keyword_t:((scales%252OR%20echelles%20OR%20%22organisation%20levels%22%20OR%20%22organization%20levels%22%20OR%20%22organisational%20levels%22%20OR%20%22organizational%20levels%22%20OR%20%22niveaux%20d%27organisation%22)%20AND%20ecolog*)&fq=producedDateY_i:%5b2000%20TO%202023%5d&fq=docType_s:(ART%20OR%20COMM%20OR%20POSTER%20OR%20COUV%20OR%20OUV%20OR%20THESE%20OR%20HDR%20OR%20PROCEEDINGS)%20OR%20docSubType_s:(PREPRINT%20OR%20RESREPORT%20OR%20FUNDREPORT)
/* parmis les notices on ne récupère que les auteurs dont les structures font parties de "577435","92114","302049","300005"
/*   Fichier excel : colonnes "Nom auteur","Type de document","Public visé","1er, dernier ou correspondant","idHAL","Orcid","hal-id","url"
/*   Développement : Pierre Pichard - wouaib.com
/*   Date : 09/2023
/*
/*******************************************************************************************************************************************/


ini_set('memory_limit', '292M');
$debug = false;
if ($debug)
	$h = fopen("debug.log","w");
/** Error reporting */
error_reporting(E_ERROR);
date_default_timezone_set('Europe/London');
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');


$noticesTraitees = array();  // stockage de toutes les publis traitées, pour remplir la section des publis non ventilées
$nbNoticeTraite=0;
$listeChamps = "halId_s,title_s,authFullName_s,authLastName_s,authFirstName_s,producedDate_s,producedDateY_i,docType_s,docSubType_s,inra_publicVise_local_s,peerReviewing_s,invitedCommunication_s,subTitle_s,bookTitle_s,journalTitle_s,volume_s,issue_s,page_s,publisher_s,doiId_s,uri_s,arxivId_s,biorxivId_s,authorityInstitution_s,number_s,serie_s,conferenceTitle_s,city_s,country_s,conferenceStartDate_s,conferenceEndDate_s,lectureName_s,reportType_s,lectureType_s,submitType_s,openAccess_bool,wosId_s,pubmedId_s,audience_s,otherType_s,authQuality_s,authIdFullName_fs,popularLevel_s,authIdHasPrimaryStructure_fs,linkExtId_s,language_s,authIdHasStructure_fs,authFullNameIdHal_fs";




// Récuperation des données du formulaire
$idHAL = htmlspecialchars(strip_tags($_POST['idhal']),    ENT_QUOTES);


if ($collection == "undefined")
	$collection = "";
if ($idHAL == "undefined")
	$idHAL = "";


@include_once("../lib/functions.php");
@include_once("../lib/functions_hceres.php");
@include_once("../lib/functions_chercheur.php");

  
	$requete = "https://api.archives-ouvertes.fr/search/inrae/?q=abstract_t:((scales%20OR%20echelles%20OR%20%22organisation%20levels%22%20OR%20%22organization%20levels%22%20OR%20%22organisational%20levels%22%20OR%20%22organizational%20levels%22%20OR%20%22niveaux%20d%27organisation%22)%20AND%20ecolog*)%20OR%20title_t:((scales%20OR%20echelles%20OR%20%22organisation%20levels%22%20OR%20%22organization%20levels%22%20OR%20%22organisational%20levels%22%20OR%20%22organizational%20levels%22%20OR%20%22niveaux%20d%27organisation%22)%20AND%20ecolog*)%20OR%20keyword_t:((scales%252OR%20echelles%20OR%20%22organisation%20levels%22%20OR%20%22organization%20levels%22%20OR%20%22organisational%20levels%22%20OR%20%22organizational%20levels%22%20OR%20%22niveaux%20d%27organisation%22)%20AND%20ecolog*)&fq=producedDateY_i:%5b2000%20TO%202023%5d&fq=docType_s:(ART%20OR%20COMM%20OR%20POSTER%20OR%20COUV%20OR%20OUV%20OR%20THESE%20OR%20HDR%20OR%20PROCEEDINGS)%20OR%20docSubType_s:(PREPRINT%20OR%20RESREPORT%20OR%20FUNDREPORT)";
	
	
	$url = $requete."&rows=2000&fl=".$listeChamps;

	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	if (isset ($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on")	{
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
		curl_setopt($ch, CURLOPT_CAINFO, "cacert.pem");
	}
	$resultats = curl_exec($ch);
	curl_close($ch);
	
	if ($debug)
		fwrite($h,print_r($resultats,true));
	
	$data = json_decode($resultats,true);
	$notices = $data["response"]["docs"];
		
	if ($debug) {
		fwrite($h,print_r($_POST,true));
		fwrite($h,$listeChamps."\n");
	}
	$dir = dirname(__FILE__);
	
	$libelles = array("Nom auteur","Type de document","Public visé","1er, dernier ou correspondant","idHAL","Orcid","hal-id","url");		
	$nbLibelles = count($libelles);		
	
	
	/** Include PHPExcel **/
	require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';

	// Create new PHPExcel object
	$workbook  = new PHPExcel();

function getOrcid($idhal){	
	$req = "https://api.archives-ouvertes.fr/ref/author/?q=idHal_s:".$idhal."&wt=json&fl=*orcidId_s";
			
	$resultat = file_get_contents($req);
	$res = json_decode($resultat);
	foreach($res->response->docs as $valeur)				
		$tabStruct[] = $valeur->orcidId_s[0];
		
	return implode(' ',array_unique($tabStruct));  
}

	
function isInrae($notice,$auteur){
	$ret = false;
	$TabLaboInrae = array("577435","92114","302049","300005");
	foreach ($notice['authIdHasStructure_fs'] as $auteurPS){	// pour chaque auteur rattaché à une structure primaire	
		//si l'auteur qu'on cherhce se trouve dans cette entrée alors on regarde si sa structure matche
		if (strpos($auteurPS, $auteur) !== false){
			// l'auteur est trouvé dans la chaine donc on peut chercher s'il y a une affiliation à souligner
			$tabAuteurAff = explode("_JoinSep_",$auteurPS);
			$affiliation = $tabAuteurAff[1];  // contient par ex : 1050246_FacetSep_Royal Veterinary
			$tabAff  = explode("_FacetSep_",$affiliation);
			$codeAff = $tabAff[0];
			if (in_array($codeAff,$TabLaboInrae))
				$ret = true;
		}
	}
	return $ret;
}	

// si l'auteur est premier dernier ou crp (ou co_first ou co_last)
function isPDC($notice,$auteur){
	global $debug;
	
	$retour=false;
	$TabLaboInrae = array("577435","92114","302049","300005");
	
	// ex : $struct = 924321-801067_FacetSep_Guillaume Fried_JoinSep_92114_FacetSep_Institut National de la Recherche Agronomique
	
	if ($debug) echo "<h3>DEBUT</h3>";
	$j=0;
	foreach ($notice['authIdHasStructure_fs'] as $struct){
		
		if ($debug) echo "<h5>AFFILIATION testée n° $j : ".$struct. " </h5>";
		
		// avant de testé l'affiliation on vérifie qu'elle correspond à l'auteur
		if (strpos($struct,$auteur) !== false) {
		
			
			$tabStruct = explode("_JoinSep_",$struct); // on sépare l'auteur de sa structure $tabAuteur[0] contient la partie auteur ex: 1643348_FacetSep_Patrick Baldet
			// $tabStruct[0] = 924321-801067_FacetSep_Guillaume Fried
			// $tabStruct[1] = 92114_FacetSep_Institut National de la Recherche Agronomique
			$partieAuteur  = explode("_FacetSep_",$tabStruct[0]);
			$partieAff = explode("_FacetSep_",$tabStruct[1]); 
			$idStructAuteur = $partieAff[0];   // contient le DOCID de la structure de l'auteur   ex : 92114
			$docidAuteur = $partieAuteur[0];   // contient le DOCID de l'auteur affilié  140517-945369
			$nomAuteur = $partieAuteur[1];     // contient le nom de l'auteur ex : Eric Guédon
			
			
			
			if (in_array($idStructAuteur,$TabLaboInrae)) {
				
				$nbAuteur = count($notice['authIdFullName_fs']);
				echo "Affiliation auteur testée : ".$idStructAuteur." trouvée parmis 577435 92114 302049 300005";
			
			
				$indexAuteur = getIndexAuteurByName($notice,$nomAuteur);				
				
				// il y a parfois plus d'affiliations que d'auteur donc il faut retourver le numéro d'auteur dans la liste pour retrouver son role
				if ($debug) echo "<br>INDEX AUTEUR : ".$indexAuteur. " pour ".$nomAuteur." <span style=\"font-size:12px\">( attention les tableaux commencent à l'indice 0 ;-)</span>";
				
				if ($indexAuteur == 0){ // on teste si c'est le premier auteur 		
					$retour=true;
					$rep.= $idStructAuteur." <span style=\"color:red\">1er auteur trouvé </span>";		
				}
				
				if ($indexAuteur == $nbAuteur-1){  // on teste si c'est le dernier auteur 		
					$retour=true;
					$rep.= $idStructAuteur." <span style=\"color:green\">dernier auteur trouvé </span>";		
				}	
				
				if ($notice['authQuality_s'][$indexAuteur] == "crp") {  // on teste l'auteur correspondant
					$retour=true;
					$rep.= $idStructAuteur." <span style=\"color:blue\">auteur correspondant trouvé </span>";
				}
				
				if ($notice['authQuality_s'][$indexAuteur] == "co_first_author") {  // on teste le premier co-auteur
					$retour=true;
					$rep.= $idStructAuteur." <span style=\"color:orange\">1er co-auteur </span>";
				}
				
				if ($notice['authQuality_s'][$indexAuteur] == "co_last_author") {  // on teste le dernier co-auteur
					$retour=true;
					$rep.= $idStructAuteur." <span style=\"color:yellow\">dernier co-auteur </span>";
				}		
				
			
				$exp = "<br>car tableau des structures : <br><pre>".print_r($notice['authIdHasStructure_fs'],true). "</pre> et tableau des roles auteur authQuality : ".print_r($notice['authQuality_s'],true); 
			   
				if ($retour)
					if ($debug) echo "<br><span style=\"font-size:20px;\">isPDC : OK pour ".$auteur." : ". $rep."</span> ".$exp." HAL-ID: ".$notice['halId_s']."<br>";
				else
					if ($debug) echo "<br><h5>isPDC : KO pour ".$auteur." ". $rep."</span> ".$exp."hal_Id: ".$notice['halId_s']."</h5>";
			}
			$j++;
		}
	}
	
	if ($debug) echo "<h3>FIN</h3>";
	
	return $retour; 	
	
}

// $auteur =  Julien Cucherousset
// $authIdHasStructure = $notice['authIdHasStructure_fs']
function isInraeV2($authIdHasStructure,$auteur){
	$ret = false;
	$TabLaboInrae = array("577435","92114","302049","300005");  // INRAE INRA IRSTEA CEMAGREF
	
	foreach ($authIdHasStructure as $struct){	// pour chaque auteur rattaché à une structure primaire	
		//si l'auteur qu'on cherhce se trouve dans cette entrée alors on regarde si sa structure matche
		if (strpos($struct, $auteur) !== false){
			// l'auteur est trouvé dans la chaine donc on peut chercher s'il y a une affiliation à souligner
			$tabAuteurAff = explode("_JoinSep_",$struct);
			$affiliation = $tabAuteurAff[1];  // contient par ex : 577435_FacetSep_Institut National de Recherche pour l’Agriculture, l’Alimentation et l’Environnement
			$tabAff  = explode("_FacetSep_",$affiliation);
			$codeAff = (int)$tabAff[0];       // contient par ex : 577435
			if (in_array($codeAff,$TabLaboInrae))
				$ret = true;
		}
	}
	return $ret;
}

// Retourve l'idhal dans un tableau de valeur comme cell-ci "Julien Cote_FacetSep_julien-cote en fonction de sa position
function getIdHalByPosition($tabId,$index){
	$val = $tabId[$index];
	$t = explode("_FacetSep_",$val);
	return $t[1];
}	
	
// ***********************************************************//
// Remplissage du document Excel
//
/////////////////////////////////////////////////////////////////////////////////////////
// ****************** une notice par ligne *******************************//
	
	if (!empty($notices)){
		$sheet = $workbook->getActiveSheet();
		$sheet->setTitle('Résultats');
		
		// remplissage de la 1ere ligne avec les libelles
		for ($i=0; $i<$nbLibelles; $i++){
			$pCoordinate = PHPExcel_Cell::stringFromColumnIndex($i) . '' . (1);
			$pValue = $libelles[$i];
			$sheet->setCellValue($pCoordinate, $pValue);
		} 
		
		$nbAuteur=0;
		$nbAuteurInrae=0;
		$nbAuteurInraePDC=0;
		$ligne=2;  // on remplit l'onglet avec les données à partir de la ligne n°2
		echo "Export alienor<br>";
		foreach ($notices as $notice){  
			$i=0;
			foreach ($notice['authFullName_s'] as $auteur) {    // authFullName_s contient par ex "Amanda de Mestre"
				
				//$nomAuteur = getNomAuteur($notice['authLastName_s'][$i],$notice['authFirstName_s'][$i],$notice['authFullName_s'][$i]);
				$nomAuteur = $notice['authFirstName_s'][$i]." ".$notice['authLastName_s'][$i];
				$nbAuteur++;			
			
				if ($debug) echo "<br><h1>AUTEUR TESTE : ".$auteur." dans la notice : ".$notice['halId_s']."</h1>";	
					
				if (isPDC($notice,$auteur)) {   // si l'auteur est premier dernier ou crp (ou co_first ou co_last)
				
					$nbAuteurInraePDC++;
					
					/* case Nom Auteur */
					$pCoordinate = PHPExcel_Cell::stringFromColumnIndex(0) . '' . ($ligne);
					$pValue = $nomAuteur;				
					$sheet->setCellValue($pCoordinate, $pValue);
					
					/* case Type de doc  */
					$pCoordinate = PHPExcel_Cell::stringFromColumnIndex(1) . '' . ($ligne);
					$pValue = getTypeDoc($notice['docType_s']);				
					$sheet->setCellValue($pCoordinate, $pValue);
					
					/* case public vise */
					$pCoordinate = PHPExcel_Cell::stringFromColumnIndex(2) . '' . ($ligne);
					$pValue = getPublicVise($notice['inra_publicVise_local_s'][0]);				
					$sheet->setCellValue($pCoordinate, $pValue);
					
					
					$indexAuteur = getIndexAuteurByName($notice,$auteur);					
					$type="";
					if ($indexAuteur==0) $type="Premier";
					if ( $indexAuteur == count($notice['authFullName_s'])-1 ) $type.=" Dernier";
					if ($notice['authQuality_s'][$indexAuteur] == 'co_first_author') $type.=" co_first_author";
					if ($notice['authQuality_s'][$indexAuteur] == 'co_last_author') $type.=" co_last_author";
					if ($notice['authQuality_s'][$indexAuteur] == 'crp') $type.=" crp";
					
					
					/* case 1er, dernier ou correspondant */
					$pCoordinate = PHPExcel_Cell::stringFromColumnIndex(3) . '' . ($ligne);
					$pValue = $type;				
					$sheet->setCellValue($pCoordinate, $pValue);					
									
					
					/* case idhal */
					$pCoordinate = PHPExcel_Cell::stringFromColumnIndex(4) . '' . ($ligne);
					$pValue = getIdHalByPosition($notice['authFullNameIdHal_fs'],$indexAuteur);				
					$sheet->setCellValue($pCoordinate, $pValue);
					$lien = "https://hal.science/search/index/q/*/authIdHal_s/".$pValue;
					$sheet->getCellByColumnAndRow(4,$ligne)->getHyperlink()->setUrl($lien);
					
					
					$orcid = getOrcid($pValue);
										
					/* case orcid */
					$pCoordinate = PHPExcel_Cell::stringFromColumnIndex(5) . '' . ($ligne);
					$pValue = $orcid;				
					$sheet->setCellValue($pCoordinate, $pValue);
					
					/* case  hal-id */
					$pCoordinate = PHPExcel_Cell::stringFromColumnIndex(6) . '' . ($ligne);
					$pValue = $notice['halId_s'];				
					$sheet->setCellValue($pCoordinate, $pValue);
					
					/* case url */
					$pCoordinate = PHPExcel_Cell::stringFromColumnIndex(7) . '' . ($ligne);
					$pValue = changeUri($notice);				
					$sheet->setCellValue($pCoordinate, $pValue);
					$sheet->getCellByColumnAndRow(7,$ligne)->getHyperlink()->setUrl($pValue);
					
					$ligne++;
				}			
				$i++;				
			}		    
			$nbNoticeTraite++;				 
		}
	}
		
	
	
	echo "Résultats de l'export :<br />";
	echo "Nombre de publications traitées : ".$nbNoticeTraite." <br> ";
	
	echo "Nombre d'auteurs total : ".$nbAuteur." <br> ";
	echo "Nombre d'auteurs INRAE PDC : ".$nbAuteurInraePDC." <br> ";
	
	
	
	$myXls = uniqid($idHAL.'_') . '.xlsx';
	$writer = new PHPExcel_Writer_Excel2007($workbook);
	$writer->setOffice2003Compatibility(true);
	
	$dir = dirname($dir).'/alienor';
	$urlFile = "/alienor/xlsx/".$myXls;
	
	
	// enregistrement du fichier xlsx  
	$writer->save($dir . '/xlsx/' . $myXls);
	
	if ($debug) {
		foreach($notices as $notice)
			fwrite($h,print_r($notice,true));
	}
	fclose($h);
	
	echo "<a href=\"".$urlFile."\" target=\"blank\">ici</a>";
	
	
?>